# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [Unreleased]
### Change
- todo make *some* effort to not write the messiest code in existence

## [1.0] - 2021-10-02
### Changed
- Generally cleaned up repository
- Inclusion of samples in repository
- DLL now tracks with LFS
- Filenames use spaces
- Migrated to moofemp-pdn-plugins/flag-rippler

## [0.1] - 2020-03-31
### Added
- Source code for file; appropriates an 18x12 flag ref for spriting
- Compiled dll
- Effect icon
- Template to use in conjunction with the effect
