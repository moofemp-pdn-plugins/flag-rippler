// Name: Moof's Flag Rippler v1.0
// Submenu: Render
// Author: MoofEMP
// Title: Flag Rippler
// Version: 1.0
// Desc: This was made to help with spriting for my Starbound More World Flags submod: https://gitlab.com/MoofEMP/starbound-more-world-flags
// URL: https://gitlab.com/moofemp-pdn-plugins/flag-rippler

void Render(Surface dst, Surface src, Rectangle rect)
{
    ColorBgra CurrentPixel;
    ColorBgra[] CurrentColumn;
    Surface intermediate=src.Clone();
    short[,] offsets=new short[63,2];
    //offsets literal encoding start
        offsets[ 0,0]= 14;   offsets[ 0,1]= 1;
        offsets[ 1,0]= 15;   offsets[ 1,1]= 2;
        offsets[ 2,0]= 16;   offsets[ 2,1]= 2;
        offsets[ 3,0]= 17;   offsets[ 3,1]= 1;
        offsets[ 4,0]= 18;   offsets[ 4,1]= 1;
        offsets[ 5,0]= 27;   offsets[ 5,1]=-1;
        offsets[ 6,0]= 28;   offsets[ 6,1]=-1;
        offsets[ 7,0]= 29;   offsets[ 7,1]=-1;
        offsets[ 8,0]= 30;   offsets[ 8,1]=-1;
        offsets[ 9,0]= 38;   offsets[ 9,1]= 1;
        offsets[10,0]= 39;   offsets[10,1]= 1;
        offsets[11,0]= 40;   offsets[11,1]= 1;
        offsets[12,0]= 41;   offsets[12,1]= 2;
        offsets[13,0]= 42;   offsets[13,1]= 2;
        offsets[14,0]= 43;   offsets[14,1]= 2;
        offsets[15,0]= 44;   offsets[15,1]= 1;
        offsets[16,0]= 51;   offsets[16,1]=-1;
        offsets[17,0]= 52;   offsets[17,1]=-1;
        offsets[18,0]= 53;   offsets[18,1]=-1;
        offsets[19,0]= 54;   offsets[19,1]=-2;
        offsets[20,0]= 55;   offsets[20,1]=-2;
        offsets[21,0]= 56;   offsets[21,1]=-2;
        offsets[22,0]= 57;   offsets[22,1]=-1;
        offsets[23,0]= 58;   offsets[23,1]=-1;
        offsets[24,0]= 59;   offsets[24,1]=-1;
        offsets[25,0]= 64;   offsets[25,1]= 1;
        offsets[26,0]= 65;   offsets[26,1]= 1;
        offsets[27,0]= 66;   offsets[27,1]= 1;
        offsets[28,0]= 67;   offsets[28,1]= 2;
        offsets[29,0]= 68;   offsets[29,1]= 3;
        offsets[30,0]= 79;   offsets[30,1]=-1;
        offsets[31,0]= 80;   offsets[31,1]=-1;
        offsets[32,0]= 81;   offsets[32,1]=-2;
        offsets[33,0]= 82;   offsets[33,1]=-2;
        offsets[34,0]= 83;   offsets[34,1]=-2;
        offsets[35,0]= 84;   offsets[35,1]=-2;
        offsets[36,0]= 85;   offsets[36,1]=-2;
        offsets[37,0]= 86;   offsets[37,1]=-1;
        offsets[38,0]= 88;   offsets[38,1]= 1;
        offsets[39,0]= 89;   offsets[39,1]= 1;
        offsets[40,0]= 90;   offsets[40,1]= 2;
        offsets[41,0]= 91;   offsets[41,1]= 2;
        offsets[42,0]= 92;   offsets[42,1]= 2;
        offsets[43,0]=104;   offsets[43,1]=-1;
        offsets[44,0]=105;   offsets[44,1]=-1;
        offsets[45,0]=106;   offsets[45,1]=-1;
        offsets[46,0]=107;   offsets[46,1]=-1;
        offsets[47,0]=108;   offsets[47,1]=-1;
        offsets[48,0]=109;   offsets[48,1]=-1;
        offsets[49,0]=110;   offsets[49,1]=-1;
        offsets[50,0]=111;   offsets[50,1]=-1;
        offsets[51,0]=112;   offsets[51,1]=-1;
        offsets[52,0]=129;   offsets[52,1]= 1;
        offsets[53,0]=130;   offsets[53,1]= 1;
        offsets[54,0]=131;   offsets[54,1]= 1;
        offsets[55,0]=132;   offsets[55,1]= 1;
        offsets[56,0]=133;   offsets[56,1]= 1;
        offsets[57,0]=134;   offsets[57,1]= 1;
        offsets[58,0]=135;   offsets[58,1]= 1;
        offsets[59,0]=137;   offsets[59,1]=-1;
        offsets[60,0]=138;   offsets[60,1]=-1;
        offsets[61,0]=139;   offsets[61,1]=-1;
        offsets[62,0]=140;   offsets[62,1]=-1;
    //offsets end
    //copy paste flag ref
    for(int y=0;y<src.Height;y++){
        for(int x=0;x<24;x++){
            for(int i=24;i<=120;i+=24){
                intermediate[x+i,y]=src[x,y];
                dst[x+i,y]=src[x,y]; //this will be underneath the offsetting stuff; without this line, the offset 0 parts wouldn't show up
            }
        }
    }
    //do the ripple stuff
    try{
        for(int i=0;i<offsets.Length;i++){
            CurrentColumn=OffsetColumn(offsets[i,1],offsets[i,0],intermediate);
            for(int j=0;j<dst.Height;j++)
                dst[offsets[i,0],j]=CurrentColumn[j];
        }
    }catch(IndexOutOfRangeException e){
        //i know this is really bad practice but i wasn't able to diagnose it and paint.net has NO DOCUMENTATION AT ALL
    }
}

void CopyPasteRange(int startx,int starty,int endx,int endy,int upperLeftCornerX,int upperLeftCornerY,Surface dst){ //not inclusive of the ends
    for(int y=starty;y<endy;y++){
        for(int x=startx;x<endx;x++)
            dst[upperLeftCornerX+x,upperLeftCornerY+y]=dst[x,y];
    }
}
ColorBgra[] GetColumn(int column,Surface src){
    ColorBgra[] dst=new ColorBgra[src.Height];
    for(int y=0;y<src.Height;y++)
        dst[y]=src[column,y];
    return dst;
}
ColorBgra[] OffsetColumn(int offset,int column,Surface src){
    if(offset==0) return GetColumn(column,src);
    if(offset<0) return OffsetColumnNegative(offset,column,src);
    ColorBgra[] dst=new ColorBgra[src.Height];
    ColorBgra CurrentPixel;
    ColorBgra[] stack=new ColorBgra[offset];
    ColorBgra temp;
    int stackIndex=0;
    for(int y=0;y<src.Height;y++){ //iterate down the column
        CurrentPixel=src[column,y];
        if(y<offset){
            stack[stackIndex]=CurrentPixel;
            stackIndex=(stackIndex+1)%offset; //the mod is so it doesn't go oob
            CurrentPixel.A=0; //make transparent
        }
        else{
            //swap stack[stackIndex] and CurrentPixel
            temp=CurrentPixel;
            CurrentPixel=stack[stackIndex];
            stack[stackIndex]=temp;
            //now iterate stackIndex
            stackIndex=(stackIndex+1)%offset;
        }
        dst[y]=CurrentPixel;
    }
    return dst;
}
ColorBgra[] OffsetColumnNegative(int offset,int column,Surface src){
    ColorBgra[] dst=new ColorBgra[src.Height];
    ColorBgra CurrentPixel;
    ColorBgra[] stack=new ColorBgra[-offset];
    ColorBgra temp;
    int stackIndex=0;
    for(int y=src.Height-1;y>=0;y--){ //iterate up
        CurrentPixel=src[column,y];
        if(y>src.Height-1-offset){ //this is also changed
            stack[stackIndex]=CurrentPixel;
            stackIndex=(stackIndex+1)%offset;
            CurrentPixel.A=0;
        }
        else{
            //swap stack[stackIndex] and CurrentPixel
            temp=CurrentPixel;
            CurrentPixel=stack[stackIndex];
            stack[stackIndex]=temp;
            //now iterate stackIndex
            stackIndex=(stackIndex+1)%offset;
        }
        dst[y]=CurrentPixel;
    }
    return dst;
}
